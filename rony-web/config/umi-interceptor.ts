import request from 'umi-request';
import { Storage } from 'src/utils/storage-util';

const setupUmiInterceptors = () => {
  const token = Storage.local.get('jhi-authenticationToken') || Storage.session.get('jhi-authenticationToken');
  console.log('token from  interceptor', token);
  request.interceptors.request.use((url, options) => {
    return {
      url: `${url}`,
      options: { ...options, Authorization: `Bearer ${token}` },
    };
  }, {global:  true});
};

export default setupUmiInterceptors;
