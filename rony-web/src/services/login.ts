import {request} from 'umi';

export type LoginParamsType = {
	username: string;
	password: string;
	isRememberMe: boolean;
	mobile: string;
	captcha: string;
	type: string;
};

export async function fakeAccountLogin(params: LoginParamsType) {
	console.log(params);
	return request<API.LoginStateType>('/api/authenticate', {
		method: 'POST',
		data: params,
	});
}

export async function getFakeCaptcha(mobile: string) {
	return request(`/api/login/captcha?mobile=${mobile}`);
}

export async function outLogin() {
	return request('/api/login/outLogin');
}
