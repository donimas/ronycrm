import {request} from 'umi';

export async function query() {
	return request<API.CurrentUser[]>('/api/users');
}

export async function queryCurrent() {
	const token = localStorage.getItem('jhi-authenticationToken') || sessionStorage.getItem('jhi-authenticationToken');
	console.log('token from query current', token);
	return request<API.CurrentUser>('/api/account', {
		headers: {
			'Authorization': `Bearer ${token}`
		}
	});
}

export async function queryNotices(): Promise<any> {
	return request<{ data: API.NoticeIconData[] }>('/api/notices');
}
